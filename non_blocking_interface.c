// A program that writes characters on a file in a non-blocking way.
// We use it to send controls to our robots.
// For further informations, see :
// https://web.archive.org/web/20170407122137/http://cc.byexamples.com/2007/04/08/non-blocking-user-input-in-loop-without-ncurses/

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/input.h>
#include <sys/select.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#define NB_ENABLE  0
#define NB_DISABLE 1

int kbhit()
{
  struct timeval tv;
  fd_set fds;
  tv.tv_sec = 0;
  tv.tv_usec = 0;
  FD_ZERO(&fds);
  FD_SET(STDIN_FILENO, &fds);
  select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
  return FD_ISSET(STDIN_FILENO, &fds);
}

// Configure the terminal to perform blocking / non-blocking read 
void nonblock(int state)
{
  struct termios ttystate;

  // get the terminal state
  tcgetattr(STDIN_FILENO, &ttystate);

  if (state == NB_ENABLE)
  {
    // turn off canonical mode
    ttystate.c_lflag &= ~ICANON;
    // minimum of number input read
    ttystate.c_cc[VMIN] = 1;
  }
  else if (state == NB_DISABLE)
  {
    // turn on canonical mode
    ttystate.c_lflag |= ICANON;
  }

  // set the terminal attributes
  tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}

// argv[1] --> "/dev/rfcommX"
// a.k.a the file which abstracts the bluetooth communication between the pc and the bluetooth receptor.
// A write to this file is a message sent to the receptor (which can be retrieved by reading the message
// available on the Serial Port of the receptor). 
int main(int argc, char* argv[])
{
  assert(argc > 1 && "Specify rfcomm (bluetooth) file");

  FILE* myFile = fopen(argv[1], "a");
  if (!myFile) {
    perror("Error: ");
    exit(0);
  }

  char c;
  int i = 0;
  // enable non-blocking read on stdin
  nonblock(NB_ENABLE);
	
  while (!i)
  {
    usleep(1);
    i = kbhit();
    if (i != 0)
    {
      c = fgetc(stdin);
      if (c == 'z' || c == 'q' || c == 's' || c == 'd' || c == 'a') {
        fputc(c, myFile);
        fflush(myFile);
      }
      printf("\n you hit %c. \n", c);
      // 'a' stop the program
      if (c == 'a') i = 1;
      else          i = 0;
    }
  }

  // disable non-blocking read on stdin
  nonblock(NB_DISABLE);

  return 0;
}
